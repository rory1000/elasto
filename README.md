ELASTO
======

Large scale parallelized 3d mesoscopic simulations of the mechanical response to shear in disordered media
